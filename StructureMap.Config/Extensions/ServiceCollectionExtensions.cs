﻿using System;
using System.Diagnostics;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace StructureMap.Config.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Service registration using StructureMap by default registry.
        /// Default registry automatically include other registries in assembly.
        /// Scanning operation is running over entry assembly by default.
        /// </summary>
        /// <param name="services">Service collection of native asp.net core container.</param>
        /// <param name="assembly">Assembly to scanning for.</param>
        /// <returns>Service provider</returns>
        public static IServiceProvider RegisterStructureMap(this IServiceCollection services, Assembly assembly)
        {
            var scanningAssembly = assembly ?? Assembly.GetEntryAssembly();

            var defaultRegistry = new DefaultRegistry(scanningAssembly.FullName);
            var container = new Container(defaultRegistry);

            Debug.WriteLine(container.WhatDidIScan());
            Debug.WriteLine(container.WhatDoIHave());

            container.Populate(services);

            return container.GetInstance<IServiceProvider>();
        }
    }
}