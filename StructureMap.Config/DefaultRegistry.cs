﻿namespace StructureMap.Config
{
    /// <summary>
    /// Default container configuration.
    /// </summary>
    public class DefaultRegistry : Registry
    {
        public DefaultRegistry(string assemblyFullName)
        {
            Scan(_ =>
            {
                _.Assembly(assemblyFullName);
                _.LookForRegistries();
                _.WithDefaultConventions();
            });
        }
    }
}