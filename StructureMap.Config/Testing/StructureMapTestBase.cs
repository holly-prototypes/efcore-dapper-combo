﻿using System.Reflection;

namespace StructureMap.Config.Testing
{
    /// <summary>
    /// Abstract base class for testing over StructureMap container.
    /// </summary>
    public abstract class StructureMapTestBase
    {
        /// <summary>
        /// Base container to creating child containers for test isolation.
        /// </summary>
        public Container BaseContainer { get; }

        protected virtual bool ForceScanCallingAssembly => false;

        protected StructureMapTestBase()
        {
            var assemblyName = Assembly.GetCallingAssembly().GetName().Name;

            if (!ForceScanCallingAssembly)
            {
                var lastIndexOfDot = assemblyName.LastIndexOf('.');
                assemblyName = assemblyName.Substring(0, lastIndexOfDot);
            }

            BaseContainer = new Container(new DefaultRegistry(assemblyName));
        }
    }
}