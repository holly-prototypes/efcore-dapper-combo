﻿using System;
using StructureMap.Graph;
using StructureMap.Pipeline;

namespace StructureMap.Config
{
    /// <summary>
    /// StructureMap Singleton lifecycle attribute for <b>Interface</b> and <b>Class</b>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class SingletonAttribute : StructureMapAttribute
    {
        public override void Alter(PluginFamily family)
        {
            family.SetLifecycleTo<SingletonLifecycle>();
        }

        public override void Alter(IConfiguredInstance instance)
        {
            instance.SetLifecycleTo<SingletonLifecycle>();
        }
    }
}