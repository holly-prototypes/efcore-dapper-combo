using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Playground.Repositories;
using Shouldly;

namespace Playground.Tests.Database
{
    /// <summary>
    /// Requires up and running MSSQL with created DB (run the app once to create it).
    /// Set connection string in appsettings.Test.json.
    /// </summary>
    [TestFixture]
    public class DatabaseTest
    {
        private IWebHost _webHost;
        private IAccountRepository _accountRepository;

        [SetUp]
        public void SetUp()
        {
            // Init DI context only once
            if (_webHost == null)
            {
                _webHost = WebHost.CreateDefaultBuilder(new[] {"--environment=Test"})
                    .UseStartup<Startup>()
                    .Build();
            }

            _accountRepository = _webHost.Services.GetService<IAccountRepository>();
        }

        [Test]
        public void AccountFindByIdTest()
        {
            _accountRepository.FindById(1).ShouldNotBeNull();
        }

        [Test]
        [TestCase("CSOB")]
        public void AccountFindByBankNameTest(string bankName)
        {
            var result = _accountRepository.FindByBankName(bankName);
            result.ShouldNotBeEmpty();
            // Assert inner join mapping
            result[0].Bank.ShouldNotBeNull();
        }

        [Test]
        [TestCase(100)]
        public void AccountFindByBalanceGreaterThanTest(decimal balance)
        {
            _accountRepository.FindByBalanceGreaterThan(balance).ShouldNotBeEmpty();
        }

        [Test]
        [TestCase("CSOB")]
        public void AccountFindByBankNameStoredProcTest(string bankName)
        {
            _accountRepository.CallAccountsByBanksStoredProc(bankName).ShouldNotBeEmpty();
        }
    }
}