using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using Playground.Database;
using Playground.Models.Persistent;

namespace Playground.Repositories
{
    /// <summary>
    /// Default implementation of <see cref="IAccountRepository"/>
    /// </summary>
    public class AccountRepository : IAccountRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public AccountRepository(AppDbContext appDbContext, IDbConnectionFactory dbConnectionFactory)
        {
            _appDbContext = appDbContext;
            _dbConnectionFactory = dbConnectionFactory;
        }

        /// <inheritdoc cref="IAccountRepository.Save"/>
        public Account Save(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));
            _appDbContext.Accounts.Add(account);
            _appDbContext.SaveChanges();
            return _appDbContext.Accounts.Find(account.Id);
        }

        /// <inheritdoc cref="IAccountRepository.FindById"/>
        public Account FindById(long id)
        {
            using (var conn = _dbConnectionFactory.DbConnection)
            {
                return conn.Get<Account>(1);
            }
        }

        /// <inheritdoc cref="IAccountRepository.FindByBankName"/>
        public IList<Account> FindByBankName(string bankName)
        {
            using (var conn = _dbConnectionFactory.DbConnection)
            {
                var sql = $"SELECT * FROM {DbConnectionFactory.ToTable(typeof(Account))} A " +
                          $"INNER JOIN {DbConnectionFactory.ToTable(typeof(Bank))} B on A.BankId = B.Id " +
                          $"WHERE B.name = @bankName";
                return conn.Query<Account, Bank, Account>(sql, (account, bank) =>
                {
                    account.Bank = bank;
                    return account;
                }, splitOn: "BankId", param: new {bankName}).ToList();
            }
        }

        /// <inheritdoc cref="IAccountRepository.FindByBalanceGreaterThan"/>
        public IList<Account> FindByBalanceGreaterThan(decimal balance)
        {
            using (var conn = _dbConnectionFactory.DbConnection)
            {
                var sql = $"SELECT * FROM {DbConnectionFactory.ToTable(typeof(Account))} " +
                          $"WHERE Balance > @balance";
                return conn.Query<Account>(sql, new {balance}).ToList();
            }
        }

        public IList<Account> CallAccountsByBanksStoredProc(string bankName)
        {
            using (var conn = _dbConnectionFactory.DbConnection)
            {
                return conn.Query<Account>("[Playground].[AccountsByBanks]", new {BankName = bankName},
                    commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}