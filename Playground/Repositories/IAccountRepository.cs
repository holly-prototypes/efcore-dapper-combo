using System.Collections.Generic;
using Playground.Models.Persistent;

namespace Playground.Repositories
{
    /// <summary>
    /// <see cref="Account"/> repository
    /// </summary>
    public interface IAccountRepository
    {
        /// <summary>
        /// Save account using EF Core
        /// </summary>
        Account Save(Account account);

        /// <summary>
        /// Find account by <paramref name="id"/> using Dapper
        /// </summary>
        Account FindById(long id);

        /// <summary>
        /// Find accounts by <paramref name="bankName"/> using Dapper
        /// </summary>
        IList<Account> FindByBankName(string bankName);

        /// <summary>
        /// Find accounts with balance greater than <paramref name="balance"/> using Dapper
        /// </summary>
        IList<Account> FindByBalanceGreaterThan(decimal balance);

        /// <summary>
        /// Call stored procedure to obtain list of Account belonging to <see cref="bankName"/>
        /// </summary>
        IList<Account> CallAccountsByBanksStoredProc(string bankName);
    }
}