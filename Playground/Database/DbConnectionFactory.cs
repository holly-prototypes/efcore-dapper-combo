using System;
using System.Data;
using System.Data.SqlClient;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;

namespace Playground.Database
{
    /// <summary>
    /// Creates disposable db connections used by Dapper
    /// </summary>
    public interface IDbConnectionFactory
    {
        IDbConnection DbConnection { get; }
    }

    /// <inheritdoc />
    /// <summary>
    /// Default implementation of <see cref="T:Playground.Database.IDbConnectionFactory" />
    /// </summary>
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private const string ConnectionString = "DefaultConnection";

        private readonly IConfiguration _configuration;

        public DbConnectionFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbConnection DbConnection => new SqlConnection(_configuration.GetConnectionString(ConnectionString));

        /// <summary>
        /// Map entity to table name for dapper SQL usage
        /// </summary>
        /// <param name="entityType">Type of entity</param>
        /// <returns>Translated db table name</returns>
        public static string ToTable(Type entityType) =>
            ((TableAttribute) entityType.GetCustomAttributes(typeof(TableAttribute), true)[0]).Name;
    }
}