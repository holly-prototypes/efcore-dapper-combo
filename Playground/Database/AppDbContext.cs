using Microsoft.EntityFrameworkCore;
using Playground.Models.Persistent;

namespace Playground.Database
{
    public sealed class AppDbContext : DbContext
    {
        public const string SchemaName = "Playground";

        public DbSet<Category> Categories { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Account> Accounts { get; set; }

        private AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            BuildBank(modelBuilder);
            BuildAccount(modelBuilder);
            BuildCategory(modelBuilder);
            AccountCategoryMapping(modelBuilder);
        }

        private static void BuildBank(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bank>(entity =>
            {
                entity.ToTable("t_Bank", SchemaName);

                entity.Property(e => e.Name).IsRequired();
            });
        }

        private static void BuildAccount(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("t_Account", SchemaName);

                entity.Property(e => e.Balance)
                    .HasColumnType("decimal(18,2)");

                entity.Property(e => e.Number)
                    .IsRequired();
            });
        }

        private static void BuildCategory(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("t_Category", SchemaName);

                entity.HasKey(c => c.Code);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(255);
            });
        }

        private static void AccountCategoryMapping(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountCategoryMapping>(entity =>
            {
                entity.ToTable("t_AccountCategoryMapping", SchemaName);

                entity.HasKey(ac => new {ac.AccountId, ac.CategoryCode});
            });
        }
    }
}