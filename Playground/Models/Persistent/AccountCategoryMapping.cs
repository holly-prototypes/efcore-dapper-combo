using Dapper.Contrib.Extensions;

namespace Playground.Models.Persistent
{
    /// <summary>
    /// M:N Account - Category helper entity
    /// </summary>
    [Table("Playground.t_AccountCategoryMapping")]
    public class AccountCategoryMapping
    {
        public long AccountId { get; set; }

        public Account Account { get; set; }

        public string CategoryCode { get; set; }

        public Category Category { get; set; }
    }
}