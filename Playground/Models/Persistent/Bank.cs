using System;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;

namespace Playground.Models.Persistent
{
    /// <summary>
    /// Bank
    /// </summary>
    [Table("Playground.t_Bank")]
    public class Bank : IEntityWithId<long>
    {
        public long Id { get; set; }

        public ICollection<Account> Accounts { get; } = new HashSet<Account>();

        public string Name { get; set; }

        public void AddAccount(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));
            account.BankId = Id;
            Accounts.Add(account);
        }
    }
}