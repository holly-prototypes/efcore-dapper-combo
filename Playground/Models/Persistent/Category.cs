using System.Collections.Generic;
using Dapper.Contrib.Extensions;

namespace Playground.Models.Persistent
{
    /// <summary>
    /// Account category M:N with Account
    /// </summary>
    [Table("Playground.t_Category")]
    public class Category
    {
        public string Code { get; set; }

        public string Value { get; set; }

        public ICollection<AccountCategoryMapping> Accounts { get; set; } = new HashSet<AccountCategoryMapping>();
    }
}