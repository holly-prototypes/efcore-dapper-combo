using System.Collections.Generic;
using Dapper.Contrib.Extensions;

namespace Playground.Models.Persistent
{
    /// <summary>
    /// Bank account N:1 with Bank
    /// </summary>
    [Table("Playground.t_Account")]
    public class Account : IEntityWithId<long>
    {
        public long Id { get; set; }

        public long BankId { get; set; }

        public Bank Bank { get; set; }

        public ICollection<AccountCategoryMapping> Categories { get; } = new HashSet<AccountCategoryMapping>();

        public int Prefix { get; set; }

        public int Number { get; set; }

        public decimal Balance { get; set; }
    }
}