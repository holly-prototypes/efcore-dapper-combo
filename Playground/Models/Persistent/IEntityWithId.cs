namespace Playground.Models.Persistent
{
    public interface IEntityWithId<TId>
    {
        TId Id { get; set; }
    }
}