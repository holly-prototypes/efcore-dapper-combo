﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Playground.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "playground");

            migrationBuilder.CreateTable(
                name: "t_Bank",
                schema: "playground",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table => { table.PrimaryKey("PK_t_Bank", x => x.Id); });

            migrationBuilder.CreateTable(
                name: "t_Category",
                schema: "playground",
                columns: table => new
                {
                    Code = table.Column<string>(maxLength: 50, nullable: false),
                    Value = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table => { table.PrimaryKey("PK_t_Category", x => x.Code); });

            migrationBuilder.CreateTable(
                name: "t_Account",
                schema: "playground",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy",
                            SqlServerValueGenerationStrategy.IdentityColumn),
                    BankId = table.Column<long>(nullable: false),
                    Prefix = table.Column<int>(nullable: false),
                    Number = table.Column<int>(nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_Account", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_Account_t_Bank_BankId",
                        column: x => x.BankId,
                        principalSchema: "playground",
                        principalTable: "t_Bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "t_AccountCategoryMapping",
                schema: "playground",
                columns: table => new
                {
                    AccountId = table.Column<long>(nullable: false),
                    CategoryCode = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_AccountCategoryMapping", x => new {x.AccountId, x.CategoryCode});
                    table.ForeignKey(
                        name: "FK_t_AccountCategoryMapping_t_Account_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "playground",
                        principalTable: "t_Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_t_AccountCategoryMapping_t_Category_CategoryCode",
                        column: x => x.CategoryCode,
                        principalSchema: "playground",
                        principalTable: "t_Category",
                        principalColumn: "Code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_t_Account_BankId",
                schema: "playground",
                table: "t_Account",
                column: "BankId");

            migrationBuilder.CreateIndex(
                name: "IX_t_AccountCategoryMapping_CategoryCode",
                schema: "playground",
                table: "t_AccountCategoryMapping",
                column: "CategoryCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_AccountCategoryMapping",
                schema: "playground");

            migrationBuilder.DropTable(
                name: "t_Account",
                schema: "playground");

            migrationBuilder.DropTable(
                name: "t_Category",
                schema: "playground");

            migrationBuilder.DropTable(
                name: "t_Bank",
                schema: "playground");
        }
    }
}