using System.Linq;
using Microsoft.EntityFrameworkCore;
using Playground.Database;
using Playground.Models.Persistent;

namespace Playground.Seeds
{
    public static class PlaygroundSeed
    {
        public static void Seed(AppDbContext dbContext)
        {
            SeedCategories(dbContext);
            dbContext.SaveChanges();
            SeedBanks(dbContext);
            dbContext.SaveChanges();
            SeedStoredProcedures(dbContext);
        }

        private static void SeedCategories(AppDbContext dbContext)
        {
            if (dbContext.Categories.Any())
            {
                // Already seeded
                return;
            }

            dbContext.Categories.AddRange(new Category
            {
                Code = "CategoryA",
                Value = "Kategorie A"
            }, new Category
            {
                Code = "CategoryB",
                Value = "Kategorie B"
            });
        }

        private static void SeedBanks(AppDbContext dbContext)
        {
            if (dbContext.Banks.Any())
            {
                // Already seeded
                return;
            }

            dbContext.Banks.AddRange(new Bank
            {
                Name = "CSOB",
                Accounts =
                {
                    new Account
                    {
                        Number = 123,
                        Prefix = 222,
                        Balance = 999,
                        Categories =
                        {
                            new AccountCategoryMapping
                            {
                                CategoryCode = "CategoryA"
                            }
                        }
                    }
                }
            });
        }

        private static void SeedStoredProcedures(AppDbContext dbContext)
        {
            dbContext.Database.ExecuteSqlCommand(
                "IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[Playground].[AccountsByBanks]')) " +
                "BEGIN " +
                "DROP PROCEDURE [Playground].[AccountsByBanks]" +
                "END");

            dbContext.Database.ExecuteSqlCommand(
                "CREATE PROCEDURE [Playground].[AccountsByBanks] @BankName nvarchar(255) " +
                "AS " +
                "SELECT * " +
                "FROM Playground.t_Account as A " +
                "INNER JOIN Playground.t_Bank as B on A.BankId = B.Id " +
                "WHERE B.Name = @BankName;");
        }
    }
}