﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Playground.Seeds;

namespace Playground
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            host.Migrate();
            host.Seed(PlaygroundSeed.Seed);
            host.Run();
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}