# EF Core + Dapper combo prototype
![alt text](logo.jpg)
## Usage
1. Configure connection string in *Playground/appsettings.Development.json* and *Playground.Tests/appsettings.Test.json*.
2. Run Playground app once to create db + seed data and stored proc
3. Run DatabaseTest.cs