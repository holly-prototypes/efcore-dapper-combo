using System.Linq;
using NUnit.Framework;
using Shouldly;
using StructureMap.Config.Testing;
using StructureMap.Config.Tests.Declarations;

namespace StructureMap.Config.Tests
{
    [TestFixture]
    [Category("StructureMap")]
    public class StructureMapTestBaseClassTest
    {
        private class StructureMapTest : StructureMapTestBase
        {
            protected override bool ForceScanCallingAssembly => true;
        }

        [Test]
        public void base_container_is_initialized()
        {
            var structureMapTest = new StructureMapTest();

            Assert.NotNull(structureMapTest.BaseContainer);
        }

        [Test]
        public void base_container_should_be_configured()
        {
            var structureMapTest = new StructureMapTest();

            structureMapTest.BaseContainer.Model.Registries.Count().ShouldBeGreaterThan(2);

            structureMapTest.BaseContainer.Model.HasDefaultImplementationFor<ISingletonService>().ShouldBeTrue();
            structureMapTest.BaseContainer.Model.HasDefaultImplementationFor<ITransientService>().ShouldBeTrue();
        }
    }
}