using NUnit.Framework;
using Shouldly;
using StructureMap.Config.Testing;
using StructureMap.Config.Tests.Declarations;
using StructureMap.Pipeline;

namespace StructureMap.Config.Tests
{
    [TestFixture]
    [Category("StructureMap")]
    public class DefaultRegistryTest : StructureMapTestBase
    {
        private readonly IContainer container;

        protected override bool ForceScanCallingAssembly => true;

        public DefaultRegistryTest()
        {
            container = BaseContainer.CreateChildContainer();
        }

        [Test]
        public void default_convention_is_working()
        {
            container.GetInstance<ITypeSpecificSingleton>().ShouldBeOfType<TypeSpecificSingleton>();
        }

        [Test]
        public void default_convention_is_working_for_singleton_service()
        {
            container.GetInstance<ISingletonService>().ShouldBeOfType<SingletonService>();
        }

        [Test]
        public void default_lifecycle_is_transient()
        {
            container.Model.For<ITransientService>().Lifecycle.ShouldBeOfType<TransientLifecycle>();

            var service = container.GetInstance<ITransientService>();
            var service2 = container.GetInstance<ITransientService>();
            var service3 = container.GetInstance<ITransientService>();

            service.ShouldNotBeSameAs(service2);
            service.ShouldNotBeSameAs(service3);
            service2.ShouldNotBeSameAs(service3);
        }

        [Test]
        public void no_default_instance_for_named_multiple_interface_services()
        {
            Assert.Throws<StructureMapConfigurationException>(() => container.GetInstance<IMultiImplService>());
        }

        [Test]
        public void get_multiple_interface_implementation_by_name()
        {
            var fooService = container.GetInstance<IMultiImplService>("fooService");
            fooService.ShouldBeOfType<FooService>();

            var barService = container.GetInstance<IMultiImplService>("barService");
            barService.ShouldBeOfType<BarService>();
        }

        [Test]
        public void ctor_dependency_injection_of_multiple_interface_implementation_by_type()
        {
            var fooBarService = container.GetInstance<FooBarService>();

            fooBarService.FooService.ShouldBeOfType<FooService>();
            fooBarService.BarService.ShouldBeOfType<BarService>();
        }
    }
}