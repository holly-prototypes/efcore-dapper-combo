using NUnit.Framework;
using Shouldly;
using StructureMap.Config.Testing;
using StructureMap.Config.Tests.Declarations;
using StructureMap.Pipeline;

namespace StructureMap.Config.Tests
{
    [TestFixture]
    [Category("StructureMap")]
    public class SingletonAttributeTest : StructureMapTestBase
    {
        private readonly IContainer container;

        protected override bool ForceScanCallingAssembly => true;

        public SingletonAttributeTest()
        {
            container = BaseContainer.CreateChildContainer();
        }

        [Test]
        public void instance_of_type_specific_singleton_is_singleton()
        {
            container.Model.For<ITypeSpecificSingleton>().Lifecycle.ShouldBeOfType<TransientLifecycle>();

            var singleton = container.GetInstance<ITypeSpecificSingleton>();
            var singleton2 = container.GetInstance<ITypeSpecificSingleton>();
            var singleton3 = container.GetInstance<ITypeSpecificSingleton>();

            singleton.ShouldBeSameAs(singleton2);
            singleton.ShouldBeSameAs(singleton3);
            singleton2.ShouldBeSameAs(singleton3);
        }

        [Test]
        public void instance_of_singleton_service_interface_is_singleton()
        {
            container.Model.For<ISingletonService>().Lifecycle.ShouldBeOfType<SingletonLifecycle>();

            var singleton = container.GetInstance<ISingletonService>();
            var singleton2 = container.GetInstance<ISingletonService>();
            var singleton3 = container.GetInstance<ISingletonService>();

            singleton.ShouldBeSameAs(singleton2);
            singleton.ShouldBeSameAs(singleton3);
            singleton2.ShouldBeSameAs(singleton3);
        }
    }
}