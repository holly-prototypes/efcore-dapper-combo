﻿namespace StructureMap.Config.Tests.Declarations
{
    public class FooBarService
    {
        public IFooService FooService { get; }
        public IBarService BarService { get; }

        public FooBarService(IFooService fooService, IBarService barService)
        {
            FooService = fooService;
            BarService = barService;
        }
    }
}