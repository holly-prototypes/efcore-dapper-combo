﻿using StructureMap.Config.Tests.Declarations;

namespace StructureMap.Config.Tests.OtherRegistries
{
    public class MultipleImplServicesRegistry : Registry
    {
        public MultipleImplServicesRegistry()
        {
            For<IMultiImplService>().Add<FooService>().Named("fooService");
            For<IMultiImplService>().Add<BarService>().Named("barService");
        }
    }
}